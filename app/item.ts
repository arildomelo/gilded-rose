
export class Item {
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

/**
 * Item types Enumeration
 */
enum ItemType{
    GENERIC = 0,
    AGED_BRIE = 1,
    BACKSTAGE_PASSES = 2,
    SULFURAS = 3,
    CONJURED  = 4

}

/**
 * Get the mapping between the Items and their type
 */
function getItemMapping(){
    let itemMapping: Array<Array<String>> = [];
    itemMapping[ItemType.AGED_BRIE] = ["Aged Brie"];
    itemMapping[ItemType.SULFURAS] = ['Sulfuras, Hand of Ragnaros', 'Sulfuras'];
    itemMapping[ItemType.BACKSTAGE_PASSES] = ['Backstage passes to a TAFKAL80ETC concert', 'Backstage passes temp'];
    itemMapping[ItemType.CONJURED] = ['Conjured prod'];

    return itemMapping;
}

const itemMapping: Array<Array<String>> = getItemMapping();

/**
 * check if the item is a "Aged Brie"
 * @param item the item to be checked
 */
export function isItemAgedBrie(item: Item){
    return itemMapping[ItemType.AGED_BRIE].indexOf(item.name) !== -1;
}

/**
 * check if the item is a "Sulfuras"
 * @param item the item to be checked
 */
export function isItemSulfuras(item: Item){
    return itemMapping[ItemType.SULFURAS].indexOf(item.name) !== -1;
}

/**
 * Check if the item is a "Backstage Pass"
 * @param item  the item to be chedked
 */
export function isItemBackstagePass(item: Item){
    return itemMapping[ItemType.BACKSTAGE_PASSES].indexOf(item.name) !== -1;
}

/**
 * Check if the item is a "Conjured"
 * @param item the item to be chedked
 */
export function isItemConjured(item: Item){
    return itemMapping[ItemType.CONJURED].indexOf(item.name) !== -1;
}
