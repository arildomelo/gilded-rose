import { Item, isItemConjured, isItemAgedBrie, isItemBackstagePass, isItemSulfuras } from "./item";

const MAX_QUALITY = 50;
const MIN_QUALITY = 0;

export class GildedRose {
    items: Array<Item>;
    
    constructor(items = [] as Array<Item>) {
        this.items = items;
    }
    /**
     * Update all the items.
     */
    updateItems() {
        this.items.forEach(item => {

            if(isItemSulfuras(item)) return;

            this.updateItemSellin(item);// item sellin should be updated before item quality update
            this.updateItemQuality(item);
            
        });
        return this.items;
    }

    /**
     * Update Item Quality based on business rules
     * @param item the Item to be updated
     */
    updateItemQuality(item){
        if(isItemBackstagePass(item)){
            if(item.sellIn < 0){
                item.quality = 0;
            }else{
                this.incItemQuality(item);
            }
        }
        else if(isItemAgedBrie(item)){
            this.incItemQuality(item);
        }else{
            this.decItemQuality(item);
        }
    }

    /**
     * Update SellIn
     * @param item the item on which the sellin will be updated 
     */
    updateItemSellin(item: Item){
        item.sellIn--;
    }

    
    /**
     * Decrement Item Quality based on business rules
     * @param item the item on which the quality will be decremented
     * @description - Business Rules
      ** Sellin decreases by 2 if less than 0
      ** Sellin decreases by 1 if greater than or equal 0
      ** Conjured items degrade in Quality twice as fast as normal items 
     */
    decItemQuality(item: Item){
        let decQtd: number = item.sellIn < 0 ? 2 : 1;
    
        if(isItemConjured(item)){
            decQtd *= 2;
        }
    
        for(let i = 0; i < decQtd && item.quality > MIN_QUALITY; i++ ){
            item.quality--;
        }
    
        return;
    }
    
    /**
     * Increment Item Quality based on business rules.
     * @param item the item on which the quality will be incremented 
     * @description
     * Business Rules
         ** "Backstage passes" Quality increasesby 3 when there are 5 days or less
         ** "Backstage passes" Quality increasesby 2 when there are 10 days or less 
         ** Quality should not be greather than MAX QUALITY
     */
    incItemQuality(item: Item){
        let incQtd: number =  1;
        
        /**
         
         */
        if(isItemBackstagePass(item)){
            if(item.sellIn < 6){
                incQtd = 3;
            }else if(item.sellIn < 11){
                incQtd = 2;
            }
        }
       
        /* */
        for(let i = 0; i < incQtd && item.quality < MAX_QUALITY; i++ ){
            item.quality++;
        }
    
        return;
    }
}