# Gilded Rose by Arildo Melo #

This is a small project part of the Wemanity challenge to refactor the code.
In this project I will use typescript code base.



## Env Requirements ##
The code was tested with Node v14.4.0
Make sure you have the same version or check dependencies compatibility

## Tests ##
Please make sure all test case are green

### Update Test Cases ###
in case you need to increase the test cases please place your spec.ts files in the ./test dir.

### to Run tests ###
run: npm test

### To run Text Test###
run: npm run testText

### Owner Contact ###

Arildo Melo - arildo10@gmail.com