import { expect } from 'chai';
import { GildedRose } from '../app/gilded-rose';
import { Item} from '../app/item';

describe('Gilded Rose', function () {

    it('should decrease sellIn', function() {
        const gildedRose = new GildedRose([ new Item('foo', 0, 0) ]);
        const items = gildedRose.updateItems();
        expect(items[0].sellIn).to.equal(-1);
    });

    it('quality should not be negative', function(){
        const gildedRose = new GildedRose([ new Item('foo', 0, 0) ]);
        const items = gildedRose.updateItems();
        expect(items[0].quality).to.equal(0);
    });

    it('quality should not be more than 50', function(){
        const gildedRose = new GildedRose([ new Item('Aged Brie', 10, 50) ]);
        const items = gildedRose.updateItems();
        expect(items[0].quality).to.equal(50);
    });

    describe('generic item', () => {
        
        it('should decrease quality by 2 after sell date passed', () => {
            const gildedRose = new GildedRose([ new Item('Foo', -1, 20) ]);
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(18);
        });
    });

    describe('Sulfuras', function(){
        const gildedRose = new GildedRose([ new Item('Sulfuras', 10, 80) ]);
        it('should not decrease quality', ()=>{
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(80);
        });

        it('should not decrease sellIn', ()=>{
            const items = gildedRose.updateItems();
            expect(items[0].sellIn).to.equal(10);
        });
    });

    describe('Aged Brie', function(){
        const gildedRose = new GildedRose([ new Item('Aged Brie', 10, 20) ]);
        it('should increase quality the older it gets', ()=>{
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(21);
        });
    });

    describe('Backstage passes', () => {
        
        it('increase quality by 2 with less than 10 days but greater than 5 days', () => {
            const gildedRose = new GildedRose([ new Item('Backstage passes temp', 9, 20) ]);
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(22);
        });

        it('increase quality by 2 with equal 10 days but greater than 5 days', () => {
            const gildedRose = new GildedRose([ new Item('Backstage passes temp', 10, 20) ]);
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(22);
        });

        it('increase quality by 3 with less than 5 days', () => {
            const gildedRose = new GildedRose([ new Item('Backstage passes temp', 4, 20) ]);
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(23);
        });

        it('increase quality by 3 with 5 days', () => {
            const gildedRose = new GildedRose([ new Item('Backstage passes temp', 5, 20) ]);
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(23);
        });

        it('should drop quality to 0 after concert', () => {
            const gildedRose = new GildedRose([ new Item('Backstage passes temp', -1, 20) ]);
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(0);
        });
    });

    describe('Conjured', function(){
        const gildedRose = new GildedRose([ new Item('Conjured prod', 10, 20) ]);
        it('should decrease quality by 2', ()=>{
            const items = gildedRose.updateItems();
            expect(items[0].quality).to.equal(18);
        });
    });
});
